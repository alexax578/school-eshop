﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Review
    {
        public int Id { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public string Contents { get; set; }
        [Required]
        public int Rating { get; set; }
    }
}
