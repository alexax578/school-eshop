﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class DeliveryInfo
    {
        public int Id { get; set; }
        [Required, MaxLength(200)]
        public string ContactInfo { get; set; }
        [MaxLength(100)]
        public string Firstname { get; set; }
        [Required, MaxLength(100)]
        public string Lastname { get; set; }
        [Required, MaxLength(1000)]
        public string Address { get; set; }
        [MaxLength(200)]
        public string Apartment { get; set; }
        [Required, MaxLength(200)]
        public string City { get; set; }
        [Required]
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
        [Required, MaxLength(20)]
        public string PostalCode { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
