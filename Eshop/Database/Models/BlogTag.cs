﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class BlogTag
    {
        public int Id { get; set; }
        [Required]
        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
        [Required]
        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
