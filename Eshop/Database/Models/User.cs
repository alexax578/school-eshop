﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class User : IdentityUser
    {
        [Required, MaxLength(200)]
        public string FirstName { get; set; }
        [Required, MaxLength(200)]
        public string LastName { get; set; }
        [Required, MaxLength(500)]
        public string Password { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public int? DeliveryInfoId { get; set; }
        public int? WishlistId { get; set; }
        public virtual DeliveryInfo DeliveryInfo { get; set; }
        public virtual Wishlist Wishlist { get; set; }
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
