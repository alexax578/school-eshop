﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class BlogPicture
    {
        public int Id { get; set; }
        [Required, MaxLength(2000)]
        public string Path { get; set; }
        public virtual ICollection<Blog> Blogs { get; set; }
    }
}
