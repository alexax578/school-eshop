﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Country
    {
        public int Id { get; set; }
        [Required, MaxLength(200)]
        public string Name { get; set; }
        public virtual ICollection<DeliveryInfo> DeliveryInfos { get; set; }
    }
}
