﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Comment
    {
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public string Contents { get; set; }
    }
}
