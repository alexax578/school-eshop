﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class ProductPicture
    {
        public int Id { get; set; }
        [Required, MaxLength(2000)]
        public string Path { get; set; }
    }
}
