﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Message
    {
        public int Id { get; set; }
        [Required, MaxLength(200)]
        public string Fullname { get; set; }
        [Required, MaxLength(200)]
        public string Email { get; set; }
        [Required, MaxLength(200)]
        public string Subject { get; set; }
        [Required]
        public string Contents { get; set; }
    }
}
