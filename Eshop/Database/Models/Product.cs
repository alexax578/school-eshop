﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required, MaxLength(200)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Stock { get; set; }
        public virtual ICollection<ProductCategory> Categories { get; set; }
        public virtual ICollection<ProductSign> Signs { get; set; }
        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<OrderProduct> Orders { get; set; }
        public virtual ICollection<WishlistProduct> Wishlists { get; set; }
    }
}
