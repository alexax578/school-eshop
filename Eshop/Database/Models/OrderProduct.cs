﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class OrderProduct
    {
        public int Id { get; set; }
        [Required]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public int PriceId { get; set; }
        public virtual Price Price { get; set; }
        [Required]
        public int Count { get; set; }
    }
}
