﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class WishlistProduct
    {
        public int Id { get; set; }
        [Required]
        public int WishlistId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int Count { get; set; }
        public virtual Wishlist Wishlist { get; set; }
        public virtual Product Product { get; set; }
    }
}
