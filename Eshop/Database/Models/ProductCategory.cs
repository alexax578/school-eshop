﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class ProductCategory
    {
        public int Id { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
