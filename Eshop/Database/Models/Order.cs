﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [Required]
        public int DeliveryInfoId { get; set; }
        public virtual DeliveryInfo DeliveryInfo { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime ShippedAt { get; set; }
        public virtual ICollection<OrderProduct> Products { get; set; }
    }
}
