﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class ProductSign
    {
        public int Id { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public int SignId { get; set; }
        public Sign Sign { get; set; }
    }
}
