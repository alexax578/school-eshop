﻿using System.Collections.Generic;

namespace Eshop.Database.Models
{
    public class Wishlist
    {
        public int Id { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<WishlistProduct> Products { get; set; }
    }
}