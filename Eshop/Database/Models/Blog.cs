﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eshop.Database.Models
{
    public class Blog
    {
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int BlogPictureId { get; set; }
        public virtual BlogPicture BlogPicture { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required, MaxLength(200)]
        public string Title { get; set; }
        [Required]
        public string Contents { get; set; }
        public virtual ICollection<BlogTag> Tags { get; set; }
    }
}
