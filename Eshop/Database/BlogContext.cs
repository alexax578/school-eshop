﻿using Eshop.Configuration;
using Eshop.Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Eshop.Database
{
    public class BlogContext : IdentityDbContext<User>
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogPicture> BlogPictures { get; set; }
        public DbSet<BlogTag> BlogTagModels { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RoleConfiguration());

            modelBuilder.Ignore<Comment>();
            modelBuilder.Ignore<Order>();
            modelBuilder.Ignore<DeliveryInfo>();
        }
    }
}
