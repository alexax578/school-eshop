﻿using Eshop.Configuration;
using Eshop.Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Eshop.Database
{
    public class EshopContext : IdentityDbContext<User>
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductCategory> ProductCategoryModels { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Sign> Signs { get; set; }
        public DbSet<ProductSign> ProductSignModels { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ProductPicture> ProductPictures { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<OrderProduct> OrderProductModels { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<DeliveryInfo> DeliveryInfos { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Wishlist> Wishlists { get; set; }
        public DbSet<WishlistProduct> WishlistProducts { get; set; }

        public EshopContext(DbContextOptions<EshopContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RoleConfiguration());

            modelBuilder.Ignore<Blog>();
        }
    }
}
