﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Eshop.Models;
using Eshop.Database.Models;
using AutoMapper;
using Eshop.Database;
using Microsoft.EntityFrameworkCore;

namespace Eshop.Controllers
{
    public class PagesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILogger<PagesController> _logger;
        private readonly EshopContext _context;

        public PagesController(IMapper mapper, ILogger<PagesController> logger, EshopContext context)
        {
            _mapper = mapper;
            _logger = logger;
            _context = context;
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact(MessageModel messageModel)
        {
            if (!ModelState.IsValid) return View(messageModel);

            var message = _mapper.Map<Message>(messageModel);

            try
            {
                _context.Messages.Add(message);
                _context.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Unable to save changes.");
            }

            return View(messageModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
