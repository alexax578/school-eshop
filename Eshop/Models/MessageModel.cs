﻿using System.ComponentModel.DataAnnotations;

namespace Eshop.Models
{
    public class MessageModel
    {
        [Required, Display(Name = "Full Name")]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required, Display(Name = "Message")]
        public string Contents { get; set; }
    }
}
