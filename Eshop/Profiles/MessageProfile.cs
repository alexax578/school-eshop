﻿using AutoMapper;
using Eshop.Database.Models;
using Eshop.Models;

namespace Eshop.Profiles
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            CreateMap<MessageModel, Message>();
        }
    }
}
